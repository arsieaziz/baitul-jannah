$(document).ready(function(){
	var width = $(window).width();

	if(width > 768){
		$('body').niceScroll({
			cursorborder: '1px solid transparent',
			scrollspeed: 200,
	    	mousescrollstep: 50,
	    	horizrailenabled:false
		});
	}

	$('.navTrigger').click(function(){
		$(this).toggleClass('active');
		$('header').find('.col-menu').toggleClass('mobile-active');
		$('header').toggleClass('active-head');
		$('body').toggleClass('overflow-y-hidden');
		$('.menu-block').toggleClass('active');
		if($(this).attr('data-click-state') == 1) {
			$(this).attr('data-click-state', 0);
			if(width > 768){
				$('body').niceScroll({cursorborder: '1px solid transparent',scrollspeed: 200,mousescrollstep: 50});
			}
		} else {
			$(this).attr('data-click-state', 1);
			if(width > 768){
				$('body').niceScroll().remove();
			}
		}
	});

	if (width < 991.98){
		$('.menu').find('ul > li').has('ul').addClass('child');
		$('.menu').find('ul > li > ul > li').has('ul > li').removeClass('child');
		$('.menu').find('ul > li > ul > li').has('ul > li').addClass('child2');
	}else{
		$('.menu').find('ul > li > ul > li').has('ul > li').addClass('child');
	}

	$('.menu').find('ul > li').has('ul').ready(function(){
		if (width < 991.98){
			$('.menu').find('ul > .child > a').removeAttr('href');
		}
	});

	$('.menu').find('ul > li > ul > li').has('ul').ready(function(){
		if (width < 991.98){
			$('.menu').find('ul > li > ul > .child > a').removeAttr('href');
			$('.menu').find('ul > li > ul > .child2 > a').removeAttr('href');
		}
	});

	$('.menu').find('ul > li').has('ul').find('a').click(function(){
		$(this).next().toggleClass('active');
		$(this).parent().toggleClass('changed');
	});

	$('.menu').find('ul > li > ul > li').has('ul').find('a').click(function(){
		$(this).next().toggleClass('active-child');
	});

	// ===== Scroll to Top ==== 
	$(window).scroll(function() {
	    if ($(this).scrollTop() >= 150) {
	        $('#return-to-top').addClass('active');
	    } else {
	        $('#return-to-top').removeClass('active');
	    }
	    if($(this).scrollTop() >= 30){
	    	$('header').addClass('bg-solid');
	    }else{
	    	$('header').removeClass('bg-solid');
	    }
	});

	$('#return-to-top').click(function(e) {
		e.preventDefault();
	    $('body,html').animate({scrollTop : 0}, 1000);
	});

	$('.lightgallery').lightGallery();

	var swiper = new Swiper('.swiper-slide', {
		loop: false,
		effect: 'fade',
		autoplay: {
			delay: 4500,
			disableOnInteraction: false,
		},
    	pagination: {
        	el: '.swiper-pagination',
        	type: 'fraction',
      	},
    	navigation: {
        	nextEl: '.slide-button-next',
        	prevEl: '.slide-button-prev',
    	},
    });

    var swipertestimoni = new Swiper('.swiper-testimoni', {
    	autoplay: {
			delay: 2500,
			disableOnInteraction: false,
		},
      	spaceBetween: 0,
      	navigation: {
			nextEl: '.swiper-testimoni-button-next',
			prevEl: '.swiper-testimoni-button-prev',
		},
		breakpoints: {
			0: {
	        	slidesPerView: 1,
	        	spaceBetween: 0,
	        },
			991.98: {
				slidesPerView: 2,
				spaceBetween: 0,
			}
		}
    });

    var swiperpartnership = new Swiper('.swiper-partnership', {
    	autoplay: {
			delay: 2500,
			disableOnInteraction: false,
		},
      	navigation: {
			nextEl: '.swiper-partnership-button-next',
			prevEl: '.swiper-partnership-button-prev',
		},
		breakpoints: {
			0: {
	        	slidesPerView: 3,
	        	spaceBetween: 30,
	        },
			767.98: {
				slidesPerView: 4,
				spaceBetween: 60,
			}
		}
    });

    var _overlay = document.getElementById('overlay');
    var _clientY = null; // remember Y position on touch start

    _overlay.addEventListener('touchstart', function (event) {
        if (event.targetTouches.length === 1) {
            // detect single touch
            _clientY = event.targetTouches[0].clientY;
        }
    }, false);

    _overlay.addEventListener('touchmove', function (event) {
        if (event.targetTouches.length === 1) {
            // detect single touch
            disableRubberBand(event);
        }
    }, false);

    function disableRubberBand(event) {
        var clientY = event.targetTouches[0].clientY - _clientY;

        if (_overlay.scrollTop === 0 && clientY > 0) {
            // element is at the top of its scroll
            event.preventDefault();
        }

        if (isOverlayTotallyScrolled() && clientY < 0) {
            //element is at the top of its scroll
            event.preventDefault();
        }
    }

    function isOverlayTotallyScrolled() {
        // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
        return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
    }

});
